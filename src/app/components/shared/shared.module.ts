import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentBoxComponent } from './content-box/content-box.component';
import { MbCalcComponent } from './mb-calc/mb-calc.component';
import { TariffBenefitsComponent } from './tariff-benefits/tariff-benefits.component';
import { AmountPanComponent } from './amount-pan/amount-pan.component';
import { FilterComponent } from './filter/filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ContentBoxComponent,
    MbCalcComponent,
    TariffBenefitsComponent,
    AmountPanComponent,
    FilterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ContentBoxComponent,
    FilterComponent
  ]
})
export class SharedModule { }
