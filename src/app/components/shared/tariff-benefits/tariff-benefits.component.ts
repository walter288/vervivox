import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Remark } from 'src/app/models/record-interface';

@Component({
  selector: 'app-tariff-benefits',
  templateUrl: './tariff-benefits.component.html',
  styleUrls: ['./tariff-benefits.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TariffBenefitsComponent implements OnInit {
  @Input() tariffBenefits: Remark[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
