import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TariffBenefitsComponent } from './tariff-benefits.component';

describe('TariffBenefitsComponent', () => {
  let component: TariffBenefitsComponent;
  let fixture: ComponentFixture<TariffBenefitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TariffBenefitsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TariffBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
