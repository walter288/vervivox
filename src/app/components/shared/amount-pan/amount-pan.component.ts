import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-amount-pan',
  templateUrl: './amount-pan.component.html',
  styleUrls: ['./amount-pan.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AmountPanComponent implements OnInit {
  @Input() costPrice: string = ''
  @Input() mobileUrl: string = ''
  @Input() desktopUrl: string = ''

  constructor() { }

  ngOnInit(): void {
  }

}
