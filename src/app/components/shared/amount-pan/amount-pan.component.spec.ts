import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmountPanComponent } from './amount-pan.component';

describe('AmountPanComponent', () => {
  let component: AmountPanComponent;
  let fixture: ComponentFixture<AmountPanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmountPanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmountPanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
