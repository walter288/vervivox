import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Offer, RecordData } from 'src/app/models/record-interface';

@Component({
  selector: 'app-content-box',
  templateUrl: './content-box.component.html',
  styleUrls: ['./content-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentBoxComponent implements OnInit {
  @Input() recordData: Offer[] = [];
  @Input() mbDetailsIst: string = '';
  @Input() mbDetailsSec: string = '';

  constructor() { }

  ngOnInit() {
  }

}
