import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-mb-calc',
  templateUrl: './mb-calc.component.html',
  styleUrls: ['./mb-calc.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MbCalcComponent implements OnInit {
  @Input() headerName: string = '';
  @Input() mbDetails: string = '';
  @Input() unicode: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
