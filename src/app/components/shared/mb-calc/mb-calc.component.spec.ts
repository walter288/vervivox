import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MbCalcComponent } from './mb-calc.component';

describe('MbCalcComponent', () => {
  let component: MbCalcComponent;
  let fixture: ComponentFixture<MbCalcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MbCalcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MbCalcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
