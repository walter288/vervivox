import { DataService } from 'src/app/services/verivox-record-api/data-service/data-service.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Offer, RecordData } from 'src/app/models/record-interface';
import { Observable, of, Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  takeAllRecords: Offer[] = [];
  subscriptions: Subscription = new Subscription();

  constructor(private verivoxDataService: DataService) {
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.verivoxDataService.allRecords$.subscribe(res => {
        console.log(res)
        this.takeAllRecords = res.offers
      })
    )
  }

  /**
   * This method triggers the sort action
   */
  takenFilterType(e: string) {
    const tempObject: Offer[] = this.takeAllRecords.slice();
    switch (e) {
      case "1":
        this.takeAllRecords = this.sortDownloadType(tempObject)
        break;

      case "2":
        this.takeAllRecords = this.sortUploadType(tempObject)
        break;

      case "3":
        this.takeAllRecords = this.sortPriceType(tempObject)
        break;

      default:
        break;
    }
  }

  /**
   * This method sorts the record by the download speed
   */
  sortDownloadType(tempObject: Offer[]) {
    return tempObject.sort((a: any, b: any) => {
      if (parseInt(a.contractTerm.downloadSpeed.content.text) < parseInt(b.contractTerm.downloadSpeed.content.text)) {
        return -1;
      } else if (parseInt(a.contractTerm.downloadSpeed.content.text) > parseInt(b.contractTerm.downloadSpeed.content.text)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  /**
   * This method sorts the record by the upload speed
   */
  sortUploadType(tempObject: Offer[]) {
    return tempObject.sort((a: any, b: any) => {
      if (parseInt(a.contractTerm.uploadSpeed.content.text) < parseInt(b.contractTerm.uploadSpeed.content.text)) {
        return -1;
      } else if (parseInt(a.contractTerm.uploadSpeed.content.text) > parseInt(b.contractTerm.uploadSpeed.content.text)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  /**
   * This method sorts the record by the price type
   */
  sortPriceType(tempObject: Offer[]) {
    return tempObject.sort((a: any, b: any) => {
      if (parseInt(a.cost.effectiveCost.content.amount) < parseInt(b.cost.effectiveCost.content.amount)) {
        return -1;
      } else if (parseInt(a.cost.effectiveCost.content.amount) > parseInt(b.cost.effectiveCost.content.amount)) {
        return 1;
      } else {
        return 0;
      }
    });
  }


  ngOnDestroy() {
    this.subscriptions.unsubscribe()
  }

}
