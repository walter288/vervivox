export interface Caption {
  text: string;
}

export interface Content {
  text: string;
}

export interface Caption2 {
  text: string;
}

export interface Content2 {
  text: string;
}

export interface Caption3 {
  text: string;
}

export interface Content3 {
}

export interface Tooltip {
  header: string;
  body: string;
}

export interface Caption4 {
  text: string;
  tooltip: Tooltip;
}

export interface Tooltip2 {
  header: string;
  body: string;
}

export interface Content4 {
  text: string;
  tooltip: Tooltip2;
}

export interface Service {
  percent: string;
  caption: Caption4;
  content: Content4;
}

export interface Tooltip3 {
  header: string;
  body: string;
}

export interface Caption5 {
  text: string;
  tooltip: Tooltip3;
}

export interface Tooltip4 {
  header: string;
  body: string;
}

export interface Content5 {
  text: string;
  tooltip: Tooltip4;
}

export interface Price {
  percent: string;
  caption: Caption5;
  content: Content5;
}

export interface Tooltip5 {
  header: string;
  body: string;
}

export interface Caption6 {
  text: string;
  tooltip: Tooltip5;
}

export interface Tooltip6 {
  header: string;
  body: string;
}

export interface Content6 {
  text: string;
  tooltip: Tooltip6;
}

export interface Switching {
  percent: string;
  caption: Caption6;
  content: Content6;
}

export interface AdditonalCustomerRatings {
  caption: Caption3;
  content: Content3;
  service: Service;
  price: Price;
  switching: Switching;
}

export interface Caption7 {
  text: string;
}

export interface Content7 {
  text: string;
}

export interface SwitchAgain {
  totalReviews: string;
  percent: string;
  providerBelongsToAProviderFamily: string;
  caption: Caption7;
  content: Content7;
}

export interface Caption8 {
  text: string;
}

export interface Content8 {
}

export interface ExplanationText {
  caption: Caption8;
  content: Content8;
}

export interface Caption9 {
}

export interface Content9 {
}

export interface Caption10 {
  text: string;
}

export interface Content10 {
  text: string;
}

export interface Comment {
  caption: Caption10;
  content: Content10;
}

export interface Caption11 {
  text: string;
}

export interface Content11 {
  text: string;
}

export interface ServiceScore {
  caption: Caption11;
  content: Content11;
}

export interface Caption12 {
  text: string;
}

export interface Content12 {
  text: string;
}

export interface PriceScore {
  caption: Caption12;
  content: Content12;
}

export interface Caption13 {
  text: string;
}

export interface Content13 {
  text: string;
}

export interface SwitchScore {
  caption: Caption13;
  content: Content13;
}

export interface Caption14 {
}

export interface Content14 {
  text: string;
}

export interface ProviderSwitchDateNote {
  caption: Caption14;
  content: Content14;
}

export interface Caption15 {
}

export interface Content15 {
  text: string;
}

export interface WouldSwitchAgain {
  caption: Caption15;
  content: Content15;
}

export interface Caption16 {
}

export interface Content16 {
  text: string;
}

export interface LastModificationDateNote {
  caption: Caption16;
  content: Content16;
}

export interface RecentUserRating {
  caption: Caption9;
  content: Content9;
  comment: Comment;
  serviceScore: ServiceScore;
  priceScore: PriceScore;
  switchScore: SwitchScore;
  providerSwitchDateNote: ProviderSwitchDateNote;
  wouldSwitchAgain: WouldSwitchAgain;
  lastModificationDateNote: LastModificationDateNote;
}

export interface UserRatings {
  caption: Caption2;
  content: Content2;
  additonalCustomerRatings: AdditonalCustomerRatings;
  switchAgain: SwitchAgain;
  explanationText: ExplanationText;
  recentUserRatings: RecentUserRating[];
}

export interface Provider {
  id: string;
  logoUrl: string;
  caption: Caption;
  content: Content;
  userRatings: UserRatings;
}

export interface Caption17 {
  text: string;
}

export interface Content17 {
  text: string;
}

export interface AccessMode {
  type: string;
}

export interface AccessModes {
  accessMode: AccessMode;
}

export interface Product {
  id: string;
  hasPhoneFlatrate: string;
  isTHomeAccess: string;
  isForStudents: string;
  positionZeroStatus: string;
  isSpecialOffer: string;
  isForBusinessCustomers: string;
  caption: Caption17;
  content: Content17;
  accessModes: AccessModes;
  numberOfPhoneLines: string;
  numberOfPhoneNumbers: string;
}

export interface Caption18 {
  text: string;
}

export interface Content18 {
  text: string;
}

export interface DownloadSpeed {
  amount: string;
  unit: string;
  caption: Caption18;
  content: Content18;
}

export interface Caption19 {
  text: string;
}

export interface Content19 {
  text: string;
}

export interface UploadSpeed {
  amount: string;
  unit: string;
  caption: Caption19;
  content: Content19;
}

export interface Caption20 {
  text: string;
}

export interface Content20 {
  text: string;
}

export interface ContractDuration {
  amount: string;
  unit: string;
  caption: Caption20;
  content: Content20;
}

export interface Caption21 {
  text: string;
}

export interface Content21 {
  text: string;
}

export interface CancellationPeriod {
  amount: string;
  unit: string;
  caption: Caption21;
  content: Content21;
}

export interface Caption22 {
  text: string;
}

export interface Content22 {
  text: string;
}

export interface ProlongationPeriod {
  amount: string;
  unit: string;
  caption: Caption22;
  content: Content22;
}

export interface Flatrate {
  type: string;
}

export interface Flatrates {
  flatrates: Flatrate[];
}

export interface Caption23 {
  text: string;
}

export interface Content23 {
  text: string;
}

export interface TrafficLimitation {
  amount: string;
  caption: Caption23;
  content: Content23;
}

export interface ContractTerm {
  downloadSpeed: DownloadSpeed;
  uploadSpeed: UploadSpeed;
  contractDuration: ContractDuration;
  cancellationPeriod: CancellationPeriod;
  prolongationPeriod: ProlongationPeriod;
  flatrates: Flatrates;
  trafficLimitation: TrafficLimitation;
}

export interface Caption24 {
  text: string;
}

export interface Content24 {
  text: string;
}

export interface Desktop {
  url: string;
  caption: Caption24;
  content: Content24;
}

export interface Caption25 {
  text: string;
}

export interface Content25 {
  text: string;
}

export interface Responsive {
  url: string;
  caption: Caption25;
  content: Content25;
}

export interface Caption26 {
  text: string;
}

export interface Content26 {
  text: string;
}

export interface NoSignUp {
  Header: string;
  Body: string;
  caption: Caption26;
  content: Content26;
}

export interface Signup {
  desktop: Desktop;
  responsive: Responsive;
  NoSignUp: NoSignUp;
}

export interface Caption27 {
  text: string;
}

export interface Content27 {
  text: string;
}

export interface CampaignSavings {
  amount: string;
  unit: string;
  caption: Caption27;
  content: Content27;
}

export interface Campaign {
  campaignId: string;
  campaignEndDate: string;
  campaignSavingsTooltip: string;
  campaignSavings: CampaignSavings;
}

export interface Caption28 {
  text: string;
}

export interface Content28 {
  text: string;
}

export interface EffectiveCost {
  amount: string;
  unit: string;
  caption: Caption28;
  content: Content28;
}

export interface Caption29 {
  text: string;
}

export interface Content29 {
  text: string;
}

export interface TotalCost {
  amount: string;
  unit: string;
  caption: Caption29;
  content: Content29;
}

export interface Caption30 {
  text: string;
}

export interface Content30 {
  text: string;
}

export interface InvoiceItem {
  amount: string;
  unit: string;
  type: string;
  caption: Caption30;
  content: Content30;
}

export interface OneTimeCost {
  totalCost: TotalCost;
  invoiceItems: InvoiceItem[];
}

export interface Caption31 {
  text: string;
}

export interface Content31 {
  text: string;
}

export interface TotalCost2 {
  amount: string;
  unit: string;
  caption: Caption31;
  content: Content31;
}

export interface Caption32 {
  text: string;
}

export interface Content32 {
  text: string;
}

export interface InvoiceItem2 {
  amount: string;
  unit: string;
  type: string;
  caption: Caption32;
  content: Content32;
}

export interface RecurringCost {
  totalCost: TotalCost2;
  invoiceItems: InvoiceItem2[];
}

export interface Caption33 {
  text: string;
}

export interface Content33 {
  text: string;
}

export interface TotalCostExplanation {
  caption: Caption33;
  content: Content33;
}

export interface CostDetails {
  oneTimeCost: OneTimeCost;
  recurringCost: RecurringCost;
  totalCostExplanation: TotalCostExplanation;
}

export interface Caption34 {
  text: string;
}

export interface Content34 {
}

export interface Caption35 {
  text: string;
}

export interface Content35 {
  text: string;
}

export interface InvoiceItem3 {
  amount: string;
  unit: string;
  type: string;
  caption: Caption35;
  content: Content35;
}

export interface CalculationAfter24Months {
  caption: Caption34;
  content: Content34;
  invoiceItems: InvoiceItem3[];
}

export interface Caption36 {
  text: string;
}

export interface Content36 {
  text: string;
}

export interface TotalCostExplanation2 {
  caption: Caption36;
  content: Content36;
}

export interface Cost {
  effectiveCost: EffectiveCost;
  costDetails: CostDetails;
  calculationAfter24Months: CalculationAfter24Months;
  totalCostExplanation: TotalCostExplanation2;
}

export interface Caption37 {
  text: string;
}

export interface Tooltip7 {
  header: string;
  body: string;
}

export interface Content37 {
  text: string;
  tooltip: Tooltip7;
}

export interface Remark {
  type: string;
  isOneTimeBonusType: string;
  printHeaderInBold: string;
  highlightMode: string;
  caption: Caption37;
  content: Content37;
}

export interface Caption38 {
  text: string;
}

export interface Content38 {
  text: string;
}

export interface Caption39 {
  text: string;
}

export interface Content39 {
  text: string;
}

export interface OneTimeCost2 {
  amount: string;
  unit: string;
  type: string;
  caption: Caption39;
  content: Content39;
}

export interface Caption40 {
  text: string;
}

export interface Content40 {
  text: string;
}

export interface RecurringCost2 {
  amount: string;
  unit: string;
  type: string;
  timeUnit: string;
  caption: Caption40;
  content: Content40;
}

export interface CostDetails2 {
  oneTimeCost: OneTimeCost2[];
  recurringCost: RecurringCost2[];
}

export interface Caption41 {
  text: string;
}

export interface Content41 {
  text: string;
}

export interface BaseFee {
  amount: string;
  unit: string;
  caption: Caption41;
  content: Content41;
}

export interface TimeFrame {
  startMonth: string;
  endMonth: string;
}

export interface Caption42 {
  text: string;
}

export interface Content42 {
  text: string;
}

export interface CancellationPeriod2 {
  amount: string;
  unit: string;
  caption: Caption42;
  content: Content42;
}

export interface ContractTerm2 {
  canBeCancelled: string;
  baseFee: BaseFee;
  timeFrame: TimeFrame;
  cancellationPeriod: CancellationPeriod2;
}

export interface Option {
  costsIncluded: string;
  type: string;
  subType: string;
  optionId: string;
  isMandatory: string;
  caption: Caption38;
  content: Content38;
  costDetails: CostDetails2;
  contractTerms: ContractTerm2[];
  shortRemark: string;
  remark: string;
}

export interface Caption43 {
  text: string;
}

export interface Content43 {
  text: string;
}

export interface AdditionalOptionsCostExplanation {
  caption: Caption43;
  content: Content43;
}

export interface AdditionalOptions {
  options: Option[];
  additionalOptionsCostExplanation: AdditionalOptionsCostExplanation;
}

export interface File {
  id: string;
  name: string;
  fileUsage: string;
  size: string;
  mimeType: string;
  url: string;
}

export interface Caption44 {
  text: string;
}

export interface Content44 {
  text: string;
}

export interface Name {
  caption: Caption44;
  content: Content44;
}

export interface Caption45 {
  text: string;
}

export interface Content45 {
  text: string;
}

export interface Price2 {
  caption: Caption45;
  content: Content45;
}

export interface Caption46 {
  text: string;
}

export interface Content46 {
}

export interface Remarks {
  caption: Caption46;
  content: Content46;
}

export interface Image {
  url: string;
}

export interface Hardware {
  type: string;
  costsIncluded: string;
  priceAmount: string;
  id: string;
  eitherOr: string;
  isMandatory: string;
  costAmount25Month: string;
  name: Name;
  price: Price2;
  remarks: Remarks;
  images: Image[];
  shortRemark: string;
}

export interface Caption47 {
  text: string;
}

export interface Content47 {
  text: string;
}

export interface HardwareCostExplanation {
  caption: Caption47;
  content: Content47;
}

export interface Hardwares {
  hardwares: Hardware[];
  hardwareCostExplanation: HardwareCostExplanation;
}

export interface Offer {
  rank: string;
  provider: Provider;
  product: Product;
  contractTerm: ContractTerm;
  signup: Signup;
  campaign: Campaign;
  cost: Cost;
  remarks: Remark[];
  additionalOptions: AdditionalOptions;
  requiresAvailabilityCheck: string;
  files: File[];
  hardwares: Hardwares;
}

export interface RecordData {
  totalResults: string;
  offers: Offer[];
}

