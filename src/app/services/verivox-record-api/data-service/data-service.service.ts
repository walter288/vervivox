import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { VerivoxApiService } from '../api-service/verivox-api.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private verivoxRecordService: VerivoxApiService) { }

  /**
  * fetches all records from the api
  * Check if observable data exist in cache
  * shareReplay allow sharing of a single subscription for all subscribers
  * @returns observable
  */
  allRecords$ = this.verivoxRecordService.getAllRecords().pipe(shareReplay(1));
}
