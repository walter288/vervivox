import { RecordData } from './../../../models/record-interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ErrorHandlerService } from '../../error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class VerivoxApiService {

  constructor(
    private http: HttpClient,
    public errorService: ErrorHandlerService
  ) { }

  /**
   * Method that performs the http get request and maps the response for the data service
   */
  getAllRecords(): Observable<RecordData> {
    const PATH = 'assets/store/store.json';
    return this.http.get<RecordData>(PATH)
      .pipe(
        catchError(this.errorService.handleError),
        map(data => data)
      );
  }

}
