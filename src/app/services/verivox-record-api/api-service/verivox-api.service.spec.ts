import { TestBed } from '@angular/core/testing';

import { VerivoxApiService } from './verivox-api.service';

describe('VerivoxApiService', () => {
  let service: VerivoxApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VerivoxApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
